import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import path from 'path';
import { VitePWA } from 'vite-plugin-pwa';

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src'),
    },
  },
  plugins: [
    react(),
    VitePWA({
      manifest: {
        short_name: 'Coin Tasker',
        name: 'Coin Tasker',
        icons: [
          {
            src: '/logo-bg.svg',
            sizes: '64x64 32x32 24x24 16x16',
            type: 'image/svg',
          },
          {
            src: '/logo-bg.svg',
            type: 'image/svg',
            sizes: '192x192',
          },
          {
            src: '/logo-bg.svg',
            type: 'image/svg',
            sizes: '512x512',
          },
        ],
        start_url: '.',
        display: 'standalone',
        theme_color: '#1a1d2d',
        background_color: '#1a1d2d',
      },
    }),
  ],
});
