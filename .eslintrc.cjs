module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": [
        "plugin:react/recommended",
        "airbnb",
        "plugin:@typescript-eslint/recommended",
        "prettier"
    ],
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 2021,
        "sourceType": "module"
    },
    "plugins": [
        "react",
        "@typescript-eslint",
        "react-hooks"
    ],
    "rules": {
        // custom
        "no-restricted-syntax": 0, // В первую очередь для работы for .. of
        "react/require-default-props": "off",
        "no-underscore-dangle": "off",
        "no-unused-expressions": "off",
        "@typescript-eslint/array-type": "error", // использовать <T>[] вместо Array<T>
        "@typescript-eslint/ban-ts-comment": "error", // запрет на ts ignore
        "@typescript-eslint/ban-tslint-comment": "error", // запрет на es lint ignore
        "@typescript-eslint/no-inferrable-types": [
            0
        ], // возможность указать тип переменной даже когда это и так понятно
        "@typescript-eslint/no-unused-vars": [
            2,
            {
                "args": "none"
            }
        ], // ошибка при наличии неиспользуемой переменной
        "react/prop-types": [
            0
        ], // позволяет нормально писать типы для пропсов
        "react/jsx-props-no-spreading": "off", // позволяет спредить пропсы
        "react/function-component-definition": [
            1,
            {
                "namedComponents": "arrow-function",
                "unnamedComponents": "arrow-function"
            }
        ], // arrow-function для компонентов
        "no-param-reassign": "off", // позволяет менять значения объекта
        "consistent-return": "off",
        "arrow-body-style": "off", // Больше мешает, чем помогает, arrow функции с блоком из "{}" проще дебажить и модифицировать в будущем
        "react/react-in-jsx-scope": "off", // у нас есть поддержка использования react без его импорта
        "react/no-array-index-key": "off", // можно юзать индекс в качестве key в map (не желательно)
        // basic (фиксят базовые проблемы eslint для react)
        "no-use-before-define": "off",
        "@typescript-eslint/no-use-before-define": [
            "error"
        ],
        "react/jsx-filename-extension": [
            "warn",
            {
                "extensions": [
                    ".tsx"
                ]
            }
        ],
        "import/extensions": [
            "error",
            "ignorePackages",
            {
                "ts": "never",
                "tsx": "never"
            }
        ],
        "react-hooks/rules-of-hooks": "error",
        "react-hooks/exhaustive-deps": "warn",
        "import/prefer-default-export": "off",
        "global-require": "off",
        // исправляет ошибку ложного срабатывания no-shadow для типов
        "no-shadow": "off",
        "@typescript-eslint/no-shadow": [
            "error"
        ],
        "import/no-extraneous-dependencies": [
            "error",
            {
                "devDependencies": true,
                "optionalDependencies": true,
                "peerDependencies": true
            }
        ]
    },
    "settings": {
        "import/resolver": {
            "typescript": {}
        }
    }
}
