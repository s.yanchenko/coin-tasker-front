import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { createApiBaseUrl } from "@/utils/api";

export const userApi = createApi({
  reducerPath: "userApi",
  baseQuery: fetchBaseQuery({
    baseUrl: createApiBaseUrl("user"),
    credentials: "include",
  }),
  endpoints: (build) => ({
    uploadAvatar: build.mutation<UploadAvatarApiResponse, UploadAvatarApiArg>({
      query: (queryArg) => ({
        url: `/upload/avatar`,
        method: "POST",
        body: queryArg.body,
      }),
    }),
    getAvatar: build.query<GetAvatarApiResponse, GetAvatarApiArg>({
      query: (queryArg) => ({ url: `/avatar/${queryArg.filename}` }),
    }),
    getBalance: build.query<GetBalanceApiResponse, GetBalanceApiArg>({
      query: () => ({ url: `/balance` }),
    }),
    getProfile: build.query<GetProfileApiResponse, GetProfileApiArg>({
      query: () => ({ url: `/profile` }),
    }),
  }),
});

export type UploadAvatarApiResponse = unknown;
export type UploadAvatarApiArg = {
  body: {
    avatar?: Blob;
  };
};
export type GetAvatarApiResponse =
  /** status 200 Успешное выполнение запроса */ Blob;
export type GetAvatarApiArg = {
  /** Наименование файла */
  filename: string;
};
export type GetBalanceApiResponse =
  /** status 200 Успешное выполнение запроса */ {
    balance?: UserBalance;
  };
export type GetBalanceApiArg = void;
export type GetProfileApiResponse =
  /** status 200 Успешное выполнение запроса */ UserModel;
export type GetProfileApiArg = void;
export type UserBalance = number;
export type UserNickname = string;
export type UserEmail = string;
export type UserAvatar = string;
export type UserModel = {
  nickname: UserNickname;
  email: UserEmail;
  balance: UserBalance;
  avatar?: UserAvatar;
};
export const {
  useUploadAvatarMutation,
  useGetAvatarQuery,
  useGetBalanceQuery,
  useGetProfileQuery,
} = userApi;
