import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { createApiBaseUrl } from "@/utils/api";

export const authApi = createApi({
  reducerPath: "authApi",
  baseQuery: fetchBaseQuery({
    baseUrl: createApiBaseUrl("auth"),
    credentials: "include",
  }),
  endpoints: (build) => ({
    registration: build.mutation<RegistrationApiResponse, RegistrationApiArg>({
      query: (queryArg) => ({
        url: `/registration`,
        method: "POST",
        body: queryArg.registrationBody,
      }),
    }),
    login: build.mutation<LoginApiResponse, LoginApiArg>({
      query: (queryArg) => ({
        url: `/login`,
        method: "POST",
        body: queryArg.loginBody,
      }),
    }),
    checkAuth: build.query<CheckAuthApiResponse, CheckAuthApiArg>({
      query: () => ({ url: `/check-auth` }),
    }),
    logout: build.query<LogoutApiResponse, LogoutApiArg>({
      query: () => ({ url: `/logout` }),
    }),
  }),
});

export type RegistrationApiResponse = unknown;
export type RegistrationApiArg = {
  registrationBody: RegistrationBody;
};
export type LoginApiResponse = unknown;
export type LoginApiArg = {
  loginBody: LoginBody;
};
export type CheckAuthApiResponse = unknown;
export type CheckAuthApiArg = void;
export type LogoutApiResponse = unknown;
export type LogoutApiArg = void;
export type UserEmail = string;
export type UserNickname = string;
export type UserPassword = string;
export type RegistrationBody = {
  email: UserEmail;
  nickname: UserNickname;
  password: UserPassword;
};
export type LoginBody = {
  email: UserEmail;
  password: UserPassword;
};
export const {
  useRegistrationMutation,
  useLoginMutation,
  useCheckAuthQuery,
  useLazyCheckAuthQuery,
  useLazyLogoutQuery,
} = authApi;
