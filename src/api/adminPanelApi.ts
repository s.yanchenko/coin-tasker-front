import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { createApiBaseUrl } from "@/utils/api";
import { TaskId } from "@/api/taskApi";

export const adminPanelApi = createApi({
  reducerPath: "adminPanelApi",
  baseQuery: fetchBaseQuery({
    baseUrl: createApiBaseUrl("admin-panel"),
    credentials: "include",
  }),
  tagTypes: ["task", "user"],
  endpoints: (build) => ({
    getTasksByFilter: build.mutation<
      GetTasksByFilterApiResponse,
      GetTasksByFilterApiArg
    >({
      query: (queryArg) => ({
        url: `/get-tasks-by-filter`,
        method: "POST",
        body: queryArg.taskFilter,
      }),
    }),
    getUsersOptions: build.query<
      GetUsersOptionsApiResponse,
      GetUsersOptionsApiArg
    >({
      query: () => ({ url: `/get-users` }),
    }),
    deleteTaskById: build.mutation<
      DeleteTaskByIdApiResponse,
      DeleteTaskByIdApiArg
    >({
      query: (queryArg) => ({
        url: `/delete-task-by-id/${queryArg.taskId}`,
        method: "DELETE",
      }),
    }),
    getTaskById: build.query<GetTaskByIdApiResponse, GetTaskByIdApiArg>({
      query: (queryArg) => ({ url: `/get-task-by-id/${queryArg.taskId}` }),
    }),
    editTaskById: build.mutation<EditTaskByIdApiResponse, EditTaskByIdApiArg>({
      query: (queryArg) => ({
        url: `/edit-task/${queryArg.taskId}`,
        method: "POST",
        body: queryArg.editTaskModel,
      }),
    }),
    getAllUsersData: build.mutation<
      GetAllUsersDataApiResponse,
      GetAllUsersDataApiArg
    >({
      query: (queryArg) => ({
        url: `/get-all-users-data`,
        method: "POST",
        body: queryArg.body,
      }),
    }),
    deleteUserById: build.mutation<
      DeleteUserByIdApiResponse,
      DeleteUserByIdApiArg
    >({
      query: (queryArg) => ({
        url: `/delete-user-by-id/${queryArg.userId}`,
        method: "DELETE",
      }),
    }),
    getUserById: build.query<GetUserByIdApiResponse, GetUserByIdApiArg>({
      query: (queryArg) => ({ url: `/get-user-by-id/${queryArg.userId}` }),
    }),
    editUserById: build.mutation<EditUserByIdApiResponse, EditUserByIdApiArg>({
      query: (queryArg) => ({
        url: `/edit-user/${queryArg.userId}`,
        method: "POST",
        body: queryArg.editUserModel,
      }),
    }),
    uploadUserAvatarByUserId: build.mutation<
      UploadUserAvatarByUserIdApiResponse,
      UploadUserAvatarByUserIdApiArg
    >({
      query: (queryArg) => ({
        url: `/upload-user-avatar/${queryArg.userId}`,
        method: "POST",
        body: queryArg.body,
      }),
    }),
    createUser: build.mutation<CreateUserApiResponse, CreateUserApiArg>({
      query: (queryArg) => ({
        url: `/create-user`,
        method: "POST",
        body: queryArg.createUserModel,
      }),
    }),
  }),
});

export type GetTasksByFilterApiResponse =
  /** status 200 Успешное выполнение запроса */ {
    items?: TaskModel[];
    pages?: number;
  };
export type GetTasksByFilterApiArg = {
  taskFilter: TaskFilter;
};
export type GetUsersOptionsApiResponse =
  /** status 200 Успешное выполнение запроса */ SelectOption[];
export type GetUsersOptionsApiArg = void;
export type DeleteTaskByIdApiResponse = unknown;
export type DeleteTaskByIdApiArg = {
  /** ID задачи */
  taskId: TaskId;
};
export type GetTaskByIdApiResponse =
  /** status 200 Успешное выполнение запроса */ TaskModel;
export type GetTaskByIdApiArg = {
  /** ID задачи */
  taskId: TaskId;
};
export type EditTaskByIdApiResponse = unknown;
export type EditTaskByIdApiArg = {
  /** ID задачи */
  taskId: TaskId;
  editTaskModel: EditTaskModel;
};
export type GetAllUsersDataApiResponse =
  /** status 200 Успешное выполнение запроса */ {
    items?: UserModel[];
    pages?: number;
  };
export type GetAllUsersDataApiArg = {
  body: {
    page: number;
  };
};
export type DeleteUserByIdApiResponse = unknown;
export type DeleteUserByIdApiArg = {
  /** ID пользователя */
  userId: UserId;
};
export type GetUserByIdApiResponse =
  /** status 200 Успешное выполнение запроса */ UserModel;
export type GetUserByIdApiArg = {
  /** ID пользователя */
  userId: UserId;
};
export type EditUserByIdApiResponse = unknown;
export type EditUserByIdApiArg = {
  /** ID пользователя */
  userId: UserId;
  editUserModel: EditUserModel;
};
export type UploadUserAvatarByUserIdApiResponse = unknown;
export type UploadUserAvatarByUserIdApiArg = {
  /** ID пользователя */
  userId: UserId;
  body: {
    avatar?: Blob;
  };
};
export type CreateUserApiResponse = unknown;
export type CreateUserApiArg = {
  createUserModel: CreateUserModel;
};
export type TaskType = "book" | "fitness" | "video" | "telegram" | "film";
export type TaskTitle = string;
export type TaskDescription = string;
export type TaskAmount = number;
export type TaskStatus = "available" | "done" | "paused" | "progress";
export type TaskDates = {
  /** Начало таймера */
  start: string;
  /** Конец таймера */
  end: string | null;
}[];
export type TaskAvailableDate = string;
export type TaskRelatedId = string;
export type TaskModel = {
  id: TaskId;
  type: TaskType;
  title: TaskTitle;
  description: TaskDescription;
  amount: TaskAmount;
  status: TaskStatus;
  dates?: TaskDates;
  taskAvailableDate?: TaskAvailableDate;
  relatedTaskId?: TaskRelatedId;
};
export type SelectOption = {
  /** Подпись элемента */
  label?: string;
  /** Значение элемента */
  value?: string | number;
};
export type TaskFilter = {
  status: SelectOption;
  type: SelectOption;
  user: SelectOption;
  /** Номер страницы */
  page: number;
  /** Значение строки поиска */
  query?: string;
};
export type EditTaskModel = {
  type: TaskType;
  title: TaskTitle;
  description: TaskDescription;
  amount: TaskAmount;
  status: TaskStatus;
  relatedTaskId?: TaskRelatedId;
};
export type UserNickname = string;
export type UserEmail = string;
export type UserBalance = number;
export type UserAvatar = string;
export type UserModel = {
  nickname: UserNickname;
  email: UserEmail;
  balance: UserBalance;
  avatar?: UserAvatar;
};
export type UserId = string;
export type EditUserModel = {
  email: UserEmail;
  nickname: UserNickname;
  balance: UserBalance;
};
export type UserPassword = string;
export type CreateUserModel = {
  email: UserEmail;
  nickname: UserNickname;
  password: UserPassword;
};
export const {
  useGetTasksByFilterMutation,
  useGetUsersOptionsQuery,
  useDeleteTaskByIdMutation,
  useGetTaskByIdQuery,
  useEditTaskByIdMutation,
  useGetAllUsersDataMutation,
  useDeleteUserByIdMutation,
  useGetUserByIdQuery,
  useEditUserByIdMutation,
  useUploadUserAvatarByUserIdMutation,
  useCreateUserMutation,
} = adminPanelApi;
