import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { createApiBaseUrl } from "@/utils/api";

export const taskApi = createApi({
  reducerPath: "taskApi",
  baseQuery: fetchBaseQuery({
    baseUrl: createApiBaseUrl("task"),
    credentials: "include",
  }),
  tagTypes: ["task"],
  endpoints: (build) => ({
    getAvailableTasks: build.query<
      GetAvailableTasksApiResponse,
      GetAvailableTasksApiArg
    >({
      query: () => ({ url: `/available-tasks` }),
      providesTags: ["task"],
    }),
    getExecutionTasks: build.query<
      GetExecutionTasksApiResponse,
      GetExecutionTasksApiArg
    >({
      query: () => ({ url: `/execution-tasks` }),
      providesTags: ["task"],
    }),
    startTaskById: build.mutation<
      StartTaskByIdApiResponse,
      StartTaskByIdApiArg
    >({
      query: (queryArg) => ({
        url: `/start-task/${queryArg.taskId}`,
        method: "POST",
        body: queryArg.body,
      }),
      invalidatesTags: ["task"],
    }),
    changeTrackerStatus: build.mutation<
      ChangeTrackerStatusApiResponse,
      ChangeTrackerStatusApiArg
    >({
      query: (queryArg) => ({
        url: `/change-tracker-status/${queryArg.taskId}`,
        method: "POST",
        body: queryArg.body,
      }),
      invalidatesTags: ["task"],
    }),
    getTaskById: build.query<GetTaskByIdApiResponse, GetTaskByIdApiArg>({
      query: (queryArg) => ({ url: `/get-task-by-id/${queryArg.taskId}` }),
      providesTags: ["task"],
    }),
  }),
});

export type GetAvailableTasksApiResponse =
  /** status 200 Успешное выполнение запроса */ TaskModel[];
export type GetAvailableTasksApiArg = void;
export type GetExecutionTasksApiResponse =
  /** status 200 Успешное выполнение запроса */ TaskModel[];
export type GetExecutionTasksApiArg = void;
export type StartTaskByIdApiResponse = unknown;
export type StartTaskByIdApiArg = {
  /** ID задачи */
  taskId: TaskId;
  body: {
    date: Date;
  };
};
export type ChangeTrackerStatusApiResponse = unknown;
export type ChangeTrackerStatusApiArg = {
  /** ID задачи */
  taskId: TaskId;
  body: {
    date: Date;
    status: TaskStatus;
  };
};
export type GetTaskByIdApiResponse =
  /** status 200 Успешное выполнение запроса */ TaskModel;
export type GetTaskByIdApiArg = {
  /** ID задачи */
  taskId: TaskId;
};
export type TaskId = string;
export type TaskType = "book" | "fitness" | "video" | "telegram" | "film";
export type TaskTitle = string;
export type TaskDescription = string;
export type TaskAmount = number;
export type TaskStatus = "available" | "done" | "paused" | "progress";
export type TaskDates = {
  /** Начало таймера */
  start: string;
  /** Конец таймера */
  end: string | null;
}[];
export type TaskAvailableDate = string;
export type TaskRelatedId = string;
export type TaskModel = {
  id: TaskId;
  type: TaskType;
  title: TaskTitle;
  description: TaskDescription;
  amount: TaskAmount;
  status: TaskStatus;
  dates?: TaskDates;
  taskAvailableDate?: TaskAvailableDate;
  relatedTaskId?: TaskRelatedId;
};
export type Date = string;
export const {
  useGetAvailableTasksQuery,
  useLazyGetAvailableTasksQuery,
  useGetExecutionTasksQuery,
  useLazyGetExecutionTasksQuery,
  useStartTaskByIdMutation,
  useChangeTrackerStatusMutation,
  useGetTaskByIdQuery,
} = taskApi;
