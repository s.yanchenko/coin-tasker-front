import { configureStore } from "@reduxjs/toolkit";
import { authApi } from "@/api/authApi";
import { userApi } from "@/api/userApi";
import { taskApi } from "@/api/taskApi";
import { adminPanelApi } from "@/api/adminPanelApi";
import { authSlice } from "@/slices/authSlice";

export const store = configureStore({
  reducer: {
    auth: authSlice.reducer,
    [adminPanelApi.reducerPath]: adminPanelApi.reducer,
    [authApi.reducerPath]: authApi.reducer,
    [userApi.reducerPath]: userApi.reducer,
    [taskApi.reducerPath]: taskApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(
      adminPanelApi.middleware,
      authApi.middleware,
      userApi.middleware,
      taskApi.middleware,
    ),
});
