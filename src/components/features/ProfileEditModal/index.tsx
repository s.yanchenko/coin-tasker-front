import Modal from "@/components/shared/Modal";
import Flex from "@/components/shared/Flex";
import Input from "@/components/shared/Input";

type ProfileEditModal = {
  isOpen: boolean;
  setIsOpen: (value: boolean) => void;
  nickname?: string;
  avatar?: string;
};

const ProfileEditModal = ({
  nickname,
  isOpen,
  setIsOpen,
  avatar,
}: ProfileEditModal) => {
  return isOpen ? (
    <Modal title="Изменить профиль" closeCallback={() => setIsOpen(false)}>
      <Flex direction="column" rowGap={24}>
        <Input placeholder={nickname} label="Никнейм" />
      </Flex>
    </Modal>
  ) : null;
};

export default ProfileEditModal;
