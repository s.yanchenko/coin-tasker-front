import Card from "@/components/shared/Card";
import CardIcon from "@/components/shared/CardIcon";
import { TTaskStatus, TTaskType } from "@/types/task";
import { useTracker } from "@/hooks/useTracker";
import Flex from "@/components/shared/Flex";
import Tag from "@/components/shared/Tag";
import { IconName } from "@/constants/icons";
import { msToTimeString } from "@/utils/date";
import LazyIcon from "@/components/shared/LazyIcon";
import { ETaskStatus } from "@/constants/task";
import Button from "@/components/shared/Button";
import { ButtonVariant } from "@/constants/button";
import { MouseEventHandler } from "react";
import cn from "./styles.module.scss";

type TaskCard = {
  id: string;
  status: TTaskStatus;
  type: TTaskType;
  title: string;
  amount: number;
  dates?: { start: string; end: string | null }[];
  isTrackerVisible?: boolean;
  isStatusButtonVisible?: boolean;
};

const TaskCard = ({
  id,
  status,
  type,
  title,
  amount,
  dates,
  isTrackerVisible,
  isStatusButtonVisible,
}: TaskCard) => {
  const { startTime, handleClickControl } = useTracker({
    id,
    dates: dates?.map((el) => ({
      start: new Date(el.start),
      end: el.end ? new Date(el.end) : null,
    })),
    status,
  });

  const handleClickStartTask: MouseEventHandler<HTMLButtonElement> = (e) => {
    e.preventDefault();
    handleClickControl();
  };

  const handleClickTimerControl: MouseEventHandler<HTMLButtonElement> = (e) => {
    e.preventDefault();
    handleClickControl();
  };

  return (
    <Card>
      <Flex direction="column" rowGap={16}>
        <Flex columnGap={16}>
          <CardIcon iconName={type} />
          <Flex justify="sb" columnGap={16}>
            <Flex direction="column" rowGap={8}>
              <span className={cn.title}>{title}</span>
              <Tag color="green" iconName={IconName.cash}>
                {amount}
              </Tag>
            </Flex>
            {isTrackerVisible && (
              <Flex direction="column" rowGap={12} alignItems="flex-end">
                <span className={cn.timer}>{msToTimeString(startTime)}</span>
                {/* eslint-disable-next-line jsx-a11y/control-has-associated-label */}
                <button
                  type="button"
                  className={cn.timer_button}
                  onClick={handleClickTimerControl}
                >
                  {status === ETaskStatus.paused ? (
                    <LazyIcon
                      name={IconName.playCircle}
                      width={32}
                      height={32}
                    />
                  ) : (
                    <LazyIcon
                      name={IconName.pauseCircle}
                      width={32}
                      height={32}
                    />
                  )}
                </button>
              </Flex>
            )}
          </Flex>
        </Flex>
        {isStatusButtonVisible && (
          <>
            {status === ETaskStatus.available && (
              <Button
                variant={ButtonVariant.primary}
                wide
                onClick={handleClickStartTask}
              >
                Запустить задачу
              </Button>
            )}
            {(status === ETaskStatus.progress ||
              status === ETaskStatus.paused) && (
              <Button variant={ButtonVariant.danger} wide>
                Завершить задачу
              </Button>
            )}
          </>
        )}
      </Flex>
    </Card>
  );
};

export default TaskCard;
