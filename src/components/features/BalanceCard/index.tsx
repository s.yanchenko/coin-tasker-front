import Flex from "@/components/shared/Flex";
import CardIcon from "@/components/shared/CardIcon";
import { IconName } from "@/constants/icons";
import Card from "@/components/shared/Card";
import LazyIcon from "@/components/shared/LazyIcon";
import { useGetBalanceQuery } from "@/api/userApi";
import { toMoneyFormat } from "@/utils/format";
import SkeletonCard from "@/components/shared/Skeletons/SkeletonCard";
import cn from "./styles.module.scss";

const BalanceCard = () => {
  const { data, isLoading } = useGetBalanceQuery();

  return isLoading ? (
    <SkeletonCard />
  ) : (
    <Card>
      <Flex columnGap={24}>
        <CardIcon iconName={IconName.wallet} />
        <Flex direction="column" rowGap={4}>
          <span className={cn.title}>Текущий баланс</span>
          <Flex columnGap={8} alignItems="center">
            <span className={cn.money}>
              {toMoneyFormat(data?.balance || 0)}
            </span>
            <LazyIcon name={IconName.cash} width={24} height={24} />
          </Flex>
        </Flex>
      </Flex>
    </Card>
  );
};

export default BalanceCard;
