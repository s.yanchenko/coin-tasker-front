import Card from "@/components/shared/Card";
import Flex from "@/components/shared/Flex";
import LazyIcon from "@/components/shared/LazyIcon";
import { IconName } from "@/constants/icons";
import { toMoneyFormat } from "@/utils/format";
import cn from "./styles.module.scss";

type ProfileCardProps = {
  nickname: string;
  balance: number;
  setIsEditModalOpen: (value: boolean) => void;
  avatar?: string;
};

const ProfileCard = ({
  nickname,
  avatar,
  balance,
  setIsEditModalOpen,
}: ProfileCardProps) => {
  return (
    <Card>
      <Flex justify="sb" columnGap={16} alignItems="flex-start">
        <Flex columnGap={16} alignItems="center">
          {avatar ? (
            <img className={cn.card_avatar} alt="User avatar" src={avatar} />
          ) : (
            <div className={cn.card_avatar_default}>
              <LazyIcon name={IconName.photograph} width={32} height={32} />
            </div>
          )}
          <Flex direction="column" rowGap={4}>
            <span className={cn.card_title}>{nickname}</span>
            <Flex columnGap={8} alignItems="center">
              <span className={cn.card_subtitle}>{toMoneyFormat(balance)}</span>
              <LazyIcon name={IconName.cash} width={32} height={32} />
            </Flex>
          </Flex>
        </Flex>
        {/* eslint-disable-next-line jsx-a11y/control-has-associated-label */}
        <button
          type="button"
          className={cn.card_edit}
          onClick={() => setIsEditModalOpen(true)}
        >
          <LazyIcon name={IconName.pencilBold} width={24} height={24} />
        </button>
      </Flex>
    </Card>
  );
};

export default ProfileCard;
