import LazyIcon from "@/components/shared/LazyIcon";
import { clsx } from "clsx";
import { ReactNode } from "react";
import { TIconName } from "@/types/icons";
import cn from "./styles.module.scss";

const Tag = ({
  children,
  iconName,
  iconPosition = "left",
  size = "sm",
  color = "secondary",
}: {
  children: ReactNode;
  iconName?: TIconName;
  iconPosition?: "left" | "right";
  size?: "xs" | "sm" | "base";
  color?:
    | "green"
    | "indigo"
    | "red"
    | "blue"
    | "cyan"
    | "orange"
    | "yellow"
    | "pink"
    | "secondary"
    | "outlined";
}) => {
  return (
    <div
      className={clsx(cn.tag, cn[color], cn[size], {
        [cn[iconPosition]]: iconPosition && iconName,
      })}
    >
      {iconName && <LazyIcon name={iconName} />}
      <span>{children}</span>
    </div>
  );
};

export default Tag;
