import { TIconName } from "@/types/icons";
import LazyIcon from "@/components/shared/LazyIcon";
import cn from "./styles.module.scss";

const CardIcon = ({ iconName }: { iconName: TIconName }) => {
  return (
    <div className={cn.card_icon}>
      <LazyIcon name={iconName} width={32} height={32} />
    </div>
  );
};

export default CardIcon;
