import { ReactNode } from "react";
import cn from "./styles.module.scss";

const Card = ({ children }: { children: ReactNode }) => {
  return <div className={cn.card}>{children}</div>;
};

export default Card;
