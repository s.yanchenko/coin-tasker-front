import LazyIcon from "@/components/shared/LazyIcon";
import { clsx } from "clsx";

import { IconName } from "@/constants/icons";
import cn from "./styles.module.scss";

const chooseCalloutIconName = (
  variant: "base" | "warning" | "info" | "danger" | "success",
) => {
  switch (variant) {
    case "warning":
      return IconName.exclamationCircle;
    case "info":
      return IconName.informationCircle;
    case "danger":
      return IconName.exclamation;
    case "success":
      return IconName.checkCircle;
    default:
      return IconName.informationCircle;
  }
};

const Callout = ({
  title,
  description,
  variant = "base",
  closeCallback,
}: {
  title: string;
  description?: string;
  variant?: "base" | "warning" | "info" | "danger" | "success";
  closeCallback?: () => void;
}) => {
  return (
    <div className={cn.callout}>
      <div className={clsx(cn.meta, cn[variant])}>
        <LazyIcon
          name={chooseCalloutIconName(variant)}
          width={24}
          height={24}
        />
        <div className={cn.meta_container}>
          <span className={cn.meta_title}>{title}</span>
          {description && (
            <span className={cn.meta_description}>{description}</span>
          )}
        </div>
      </div>
      <button type="button" className={cn.close} onClick={closeCallback}>
        Закрыть
      </button>
    </div>
  );
};

export default Callout;
