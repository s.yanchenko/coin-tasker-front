import { ReactNode } from "react";
import { useResize } from "@/hooks/useResize";
import BottomNavigation from "@/components/shared/BottomNavigation";
import cn from "./styles.module.scss";

const Layout = ({ children }: { children: ReactNode }) => {
  const { height } = useResize();

  return (
    <div className={cn.layout} style={{ height: `${height}px` }}>
      <main className={cn.layout_main}>{children}</main>
      <nav className={cn.layout_nav}>
        <BottomNavigation />
      </nav>
    </div>
  );
};

export default Layout;
