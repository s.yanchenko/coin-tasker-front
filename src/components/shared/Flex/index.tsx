import { clsx } from "clsx";
import { ReactNode } from "react";
import cn from "./styles.module.scss";

type FlexProps = {
  children: ReactNode;
  direction?: "column" | "row";
  justify?: "sb" | "center" | "sa";
  rowGap?: number;
  columnGap?: number;
  alignItems?: "center" | "flex-start" | "flex-end";
};

const Flex = ({
  direction = "row",
  children,
  justify,
  rowGap,
  columnGap,
  alignItems,
}: FlexProps) => {
  return (
    <div
      style={{ rowGap, columnGap, alignItems }}
      className={clsx(cn.flex, cn[direction], {
        [cn[justify!]]: justify,
      })}
    >
      {children}
    </div>
  );
};

export default Flex;
