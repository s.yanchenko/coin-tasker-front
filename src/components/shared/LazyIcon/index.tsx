import { lazy, Suspense } from "react";
import SkeletonIcon from "@/components/shared/Skeletons/SkeletonIcon";
import { IconName } from "@/constants/icons";
import { TIconName } from "@/types/icons";

const LazyArrowDownIcon = lazy(() => import("@/assets/icons/ArrowDownIcon"));
const LazyArrowLeftIcon = lazy(() => import("@/assets/icons/ArrowLeftIcon"));
const LazyArrowRightIcon = lazy(() => import("@/assets/icons/ArrowRightIcon"));
const LazyArrowUpIcon = lazy(() => import("@/assets/icons/ArrowUpIcon"));
const LazyBookOpenIcon = lazy(() => import("@/assets/icons/BookOpenIcon"));
const LazyCashIcon = lazy(() => import("@/assets/icons/CashIcon"));
const LazyCheckmarkIcon = lazy(() => import("@/assets/icons/CheckmarkIcon"));
const LazyCheckmarkSmallIcon = lazy(
  () => import("@/assets/icons/CheckmarkSmallIcon"),
);
const LazyCrossIcon = lazy(() => import("@/assets/icons/CrossIcon"));
const LazyDumbbellIcon = lazy(() => import("@/assets/icons/DumbbellIcon"));
const LazyHomeIcon = lazy(() => import("@/assets/icons/HomeIcon"));
const LazyNotebookIcon = lazy(() => import("@/assets/icons/NotebookIcon"));
const LazyPauseIcon = lazy(() => import("@/assets/icons/PauseIcon"));
const LazyPlayIcon = lazy(() => import("@/assets/icons/PlayIcon"));
const LazyUserIcon = lazy(() => import("@/assets/icons/UserIcon"));
const LazyCheckCircleIcon = lazy(
  () => import("@/assets/icons/CheckCircleIcon"),
);
const LazyExclamationCircleIcon = lazy(
  () => import("@/assets/icons/ExclamationCircleIcon"),
);
const LazyExclamationIcon = lazy(
  () => import("@/assets/icons/ExclamationIcon"),
);
const LazyInformationCircleIcon = lazy(
  () => import("@/assets/icons/InformationCircleIcon"),
);
const LazyLoaderIcon = lazy(() => import("@/assets/icons/LoaderIcon"));
const LazyTelegramIcon = lazy(() => import("@/assets/icons/TelegramIcon"));
const LazyFilmIcon = lazy(() => import("@/assets/icons/FilmIcon"));
const LazyWalletIcon = lazy(() => import("@/assets/icons/WalletIcon"));
const LazyWalletBoldIcon = lazy(() => import("@/assets/icons/WalletBoldIcon"));
const LazyUserBoldIcon = lazy(() => import("@/assets/icons/UserBoldIcon"));
const LazyHomeBoldIcon = lazy(() => import("@/assets/icons/HomeBoldIcon"));
const LazyNotebookBoldIcon = lazy(
  () => import("@/assets/icons/NotebookBoldIcon"),
);
const LazyClipboardListIcon = lazy(
  () => import("@/assets/icons/ClipboardListIcon"),
);
const LazyClipboardListBoldIcon = lazy(
  () => import("@/assets/icons/ClipboardListBoldIcon"),
);
const LazyPlusCircleIcon = lazy(() => import("@/assets/icons/PlusCircleIcon"));
const LazyPlusCircleBoldIcon = lazy(
  () => import("@/assets/icons/PlusCircleBoldIcon"),
);
const LazyLockClosedBold = lazy(() => import("@/assets/icons/LockClosedBold"));
const LazyPlayCircleIcon = lazy(() => import("@/assets/icons/PlayCircleIcon"));
const LazyPencilBoldIcon = lazy(() => import("@/assets/icons/PencilBoldIcon"));
const LazyPhotographIcon = lazy(() => import("@/assets/icons/PhotographIcon"));
const LazyPauseCircleIcon = lazy(
  () => import("@/assets/icons/PauseCircleIcon"),
);

const chooseIcon = (name: TIconName, width?: number, height?: number) => {
  switch (name) {
    case IconName.arrowDown:
      return <LazyArrowDownIcon width={width} height={height} />;
    case IconName.arrowLeft:
      return <LazyArrowLeftIcon width={width} height={height} />;
    case IconName.arrowRight:
      return <LazyArrowRightIcon width={width} height={height} />;
    case IconName.arrowUp:
      return <LazyArrowUpIcon width={width} height={height} />;
    case IconName.book:
      return <LazyBookOpenIcon width={width} height={height} />;
    case IconName.cash:
      return <LazyCashIcon width={width} height={height} />;
    case IconName.checkmark:
      return <LazyCheckmarkIcon width={width} height={height} />;
    case IconName.checkmarkSmall:
      return <LazyCheckmarkSmallIcon width={width} height={height} />;
    case IconName.cross:
      return <LazyCrossIcon width={width} height={height} />;
    case IconName.fitness:
      return <LazyDumbbellIcon width={width} height={height} />;
    case IconName.home:
      return <LazyHomeIcon width={width} height={height} />;
    case IconName.notebook:
      return <LazyNotebookIcon width={width} height={height} />;
    case IconName.pause:
      return <LazyPauseIcon width={width} height={height} />;
    case IconName.play:
    case IconName.video:
      return <LazyPlayIcon width={width} height={height} />;
    case IconName.user:
      return <LazyUserIcon width={width} height={height} />;
    case IconName.checkCircle:
      return <LazyCheckCircleIcon width={width} height={height} />;
    case IconName.exclamationCircle:
      return <LazyExclamationCircleIcon width={width} height={height} />;
    case IconName.exclamation:
      return <LazyExclamationIcon width={width} height={height} />;
    case IconName.informationCircle:
      return <LazyInformationCircleIcon width={width} height={height} />;
    case IconName.loader:
      return <LazyLoaderIcon width={width} height={height} />;
    case IconName.telegram:
      return <LazyTelegramIcon width={width} height={height} />;
    case IconName.film:
      return <LazyFilmIcon width={width} height={height} />;
    case IconName.wallet:
      return <LazyWalletIcon width={width} height={height} />;
    case IconName.walletBold:
      return <LazyWalletBoldIcon width={width} height={height} />;
    case IconName.userBold:
      return <LazyUserBoldIcon width={width} height={height} />;
    case IconName.homeBold:
      return <LazyHomeBoldIcon width={width} height={height} />;
    case IconName.notebookBold:
      return <LazyNotebookBoldIcon width={width} height={height} />;
    case IconName.clipboardList:
      return <LazyClipboardListIcon width={width} height={height} />;
    case IconName.clipboardBoldList:
      return <LazyClipboardListBoldIcon width={width} height={height} />;
    case IconName.plusCircle:
      return <LazyPlusCircleIcon width={width} height={height} />;
    case IconName.plusCircleBold:
      return <LazyPlusCircleBoldIcon width={width} height={height} />;
    case IconName.lockClosedBold:
      return <LazyLockClosedBold width={width} height={height} />;
    case IconName.playCircle:
      return <LazyPlayCircleIcon width={width} height={height} />;
    case IconName.pauseCircle:
      return <LazyPauseCircleIcon width={width} height={height} />;
    case IconName.pencilBold:
      return <LazyPencilBoldIcon width={width} height={height} />;
    case IconName.photograph:
      return <LazyPhotographIcon width={width} height={height} />;
    default:
      return null;
  }
};

const LazyIcon = ({
  name,
  width,
  height,
}: {
  name: TIconName;
  width?: number;
  height?: number;
}) => {
  return (
    <Suspense fallback={<SkeletonIcon width={width} height={height} />}>
      {chooseIcon(name, width, height)}
    </Suspense>
  );
};

export default LazyIcon;
