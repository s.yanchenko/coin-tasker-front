import { ReactNode } from "react";
import { clsx } from "clsx";
import cn from "./styles.module.scss";

const Heading = ({
  children,
  size = 1,
}: {
  children: ReactNode;
  size?: 1 | 2 | 3 | 4;
}) => {
  return (
    <span className={clsx(cn.heading, cn[`heading_${size}`])}>{children}</span>
  );
};

export default Heading;
