import { clsx } from "clsx";
import cn from "../styles.module.scss";

const SkeletonBlock = ({
  width,
  height,
}: {
  width: number;
  height: number;
}) => {
  return (
    <div
      style={{ minWidth: width, minHeight: height }}
      className={clsx(cn.skeleton, cn.block)}
    />
  );
};

export default SkeletonBlock;
