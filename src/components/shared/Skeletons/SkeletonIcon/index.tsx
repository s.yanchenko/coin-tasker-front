import { clsx } from 'clsx';
import cn from '@/components/shared/Skeletons/styles.module.scss';

const SkeletonIcon = ({ width, height }: { width?: number; height?: number }) => {
  return (
    <div
      style={{ minWidth: width ? `${width}px` : undefined, minHeight: height ? `${height}px` : undefined }}
      className={clsx(cn.skeleton, cn.icon)}
    />
  );
};

export default SkeletonIcon;
