import { clsx } from "clsx";
import cn from "../styles.module.scss";

const SkeletonCard = () => {
  return <div className={clsx(cn.skeleton, cn.card)} />;
};

export default SkeletonCard;
