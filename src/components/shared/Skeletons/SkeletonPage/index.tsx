import SkeletonIcon from '@/components/shared/Skeletons/SkeletonIcon';
import { clsx } from 'clsx';
import cn from '../styles.module.scss';

const SkeletonPage = () => {
  return (
    <div className={cn.layout} style={{ height: `${window.innerHeight}px` }}>
      <main className={cn.layout_main} />
      <div className={cn.nav_container}>
        <div className={cn.nav}>
          <div className={cn.nav_element}>
            <SkeletonIcon width={24} height={24} />
          </div>
          <div className={cn.nav_element}>
            <SkeletonIcon width={24} height={24} />
          </div>
          <div className={clsx(cn.nav_element, cn.nav_element_circled)}>
            <SkeletonIcon width={24} height={24} />
          </div>
          <div className={cn.nav_element}>
            <SkeletonIcon width={24} height={24} />
          </div>
          <div className={cn.nav_element}>
            <SkeletonIcon width={24} height={24} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default SkeletonPage;
