/* eslint-disable react/button-has-type */
import { MouseEventHandler, ReactNode } from "react";
import { TIconName } from "@/types/icons";
import { ButtonVariantType } from "@/types/button";
import LazyIcon from "@/components/shared/LazyIcon";
import { clsx } from "clsx";
import Loader from "@/components/shared/Loader";
import cn from "./styles.module.scss";

const Button = ({
  children,
  variant,
  onClick,
  padding = "relaxed",
  fontSize = "base",
  type = "button",
  disabled,
  iconName,
  isLoading,
  wide,
}: {
  children: ReactNode;
  variant: ButtonVariantType;
  onClick?: MouseEventHandler<HTMLButtonElement>;
  padding?: "relaxed" | "compact";
  fontSize?: "xs" | "sm" | "base" | "lg";
  type?: "submit" | "button" | "reset";
  disabled?: boolean;
  iconName?: TIconName;
  isLoading?: boolean;
  wide?: boolean;
}) => {
  return (
    <button
      type={type}
      disabled={disabled}
      onClick={!isLoading ? onClick : undefined}
      className={clsx(cn.button, cn[variant], cn[fontSize], cn[padding], {
        [cn.button_wide]: wide,
      })}
    >
      {iconName && <LazyIcon name={iconName} width={24} height={24} />}
      {children}
      {isLoading && <Loader width={24} height={24} />}
    </button>
  );
};

export default Button;
