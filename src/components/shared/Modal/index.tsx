import { createPortal } from "react-dom";
import { ReactNode } from "react";
import Flex from "@/components/shared/Flex";
import LazyIcon from "@/components/shared/LazyIcon";
import { IconName } from "@/constants/icons";
import Button from "@/components/shared/Button";
import { ButtonVariant } from "@/constants/button";
import cn from "./styles.module.scss";

type ModalProps = {
  title: string;
  children: ReactNode;
  applyCallback?: () => void;
  closeCallback?: () => void;
  applyButtonText?: string;
};

const Modal = ({
  title,
  children,
  applyCallback,
  closeCallback,
  applyButtonText,
}: ModalProps) => {
  return createPortal(
    <>
      <div className={cn.modal_backdrop} />
      <div className={cn.modal_wrapper}>
        <div className={cn.modal_container}>
          <div className={cn.modal_header}>
            <Flex justify="sb" alignItems="center">
              <span className={cn.modal_header_title}>{title}</span>
              {/* eslint-disable-next-line jsx-a11y/control-has-associated-label */}
              <button
                type="button"
                className={cn.modal_header_close}
                onClick={closeCallback}
              >
                <LazyIcon name={IconName.cross} width={20} height={20} />
              </button>
            </Flex>
          </div>
          <div className={cn.modal_content}>{children}</div>
          <div className={cn.modal_footer}>
            <Button
              fontSize="sm"
              variant={ButtonVariant.secondary}
              onClick={closeCallback}
            >
              Отменить
            </Button>
            <Button
              fontSize="sm"
              variant={ButtonVariant.primary}
              onClick={applyCallback}
            >
              {applyButtonText}
            </Button>
          </div>
        </div>
      </div>
    </>,
    document.body,
  );
};

export default Modal;
