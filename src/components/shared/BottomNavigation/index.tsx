import LazyIcon from "@/components/shared/LazyIcon";
import { NavLink, useLocation } from "react-router-dom";
import { clsx } from "clsx";
import { Pages } from "@/constants/routes/pages";
import { IconName } from "@/constants/icons";
import cn from "./styles.module.scss";

const BottomNavigation = () => {
  const location = useLocation();

  return (
    <div className={cn.container}>
      <NavLink
        to={Pages.home}
        className={({ isActive }) =>
          clsx(cn.nav_element, { [cn.nav_element_active]: isActive })
        }
      >
        <LazyIcon
          name={
            location.pathname === Pages.home ? IconName.homeBold : IconName.home
          }
          width={24}
          height={24}
        />
      </NavLink>
      <NavLink
        to={Pages.balance}
        className={({ isActive }) =>
          clsx(cn.nav_element, { [cn.nav_element_active]: isActive })
        }
      >
        <LazyIcon
          name={
            location.pathname === Pages.balance
              ? IconName.walletBold
              : IconName.wallet
          }
          width={24}
          height={24}
        />
      </NavLink>
      <NavLink
        to={Pages.tasks}
        className={({ isActive }) =>
          clsx(cn.nav_element, cn.nav_circled, {
            [cn.nav_element_active]: isActive,
          })
        }
      >
        <LazyIcon
          name={
            location.pathname === Pages.tasks
              ? IconName.clipboardBoldList
              : IconName.clipboardList
          }
          width={24}
          height={24}
        />
      </NavLink>
      <NavLink
        to={Pages.proposeTask}
        className={({ isActive }) =>
          clsx(cn.nav_element, { [cn.nav_element_active]: isActive })
        }
      >
        <LazyIcon
          name={
            location.pathname === Pages.proposeTask
              ? IconName.plusCircleBold
              : IconName.plusCircle
          }
          width={24}
          height={24}
        />
      </NavLink>
      <NavLink
        to={Pages.profile}
        className={({ isActive }) =>
          clsx(cn.nav_element, { [cn.nav_element_active]: isActive })
        }
      >
        <LazyIcon
          name={
            location.pathname === Pages.profile
              ? IconName.userBold
              : IconName.user
          }
          width={24}
          height={24}
        />
      </NavLink>
    </div>
  );
};

export default BottomNavigation;
