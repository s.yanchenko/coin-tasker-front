import LazyIcon from "@/components/shared/LazyIcon";
import { DetailedHTMLProps, InputHTMLAttributes, useId } from "react";
import { clsx } from "clsx";
import { TIconName } from "@/types/icons";
import cn from "./styles.module.scss";

type InputProps = DetailedHTMLProps<
  InputHTMLAttributes<HTMLInputElement>,
  HTMLInputElement
> & {
  iconName?: TIconName;
  label?: string;
  subText?: string;
  isInvalid?: boolean;
};

const Input = (props: InputProps) => {
  const inputId = useId();

  const { iconName, label, subText, isInvalid, ...rest } = props;

  return (
    <div className={cn.container}>
      {label && (
        <label className={cn.label} htmlFor={inputId}>
          {label}
        </label>
      )}
      <div
        className={clsx(cn.input_container, {
          [cn.input_container_error]: isInvalid,
        })}
      >
        <input
          id={inputId}
          {...rest}
          className={clsx(cn.input, {
            [cn.input_error]: isInvalid,
            [cn.input_withIcon]: !!iconName,
          })}
        />
        {iconName && (
          <div className={cn.icon}>
            <LazyIcon name={iconName} width={20} height={20} />
          </div>
        )}
      </div>
      {subText && <span className={cn.subText}>{subText}</span>}
    </div>
  );
};

export default Input;
