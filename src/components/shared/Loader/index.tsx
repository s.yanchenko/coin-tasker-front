import LoaderIcon from '@/assets/icons/LoaderIcon';
import cn from './styles.module.scss';

const Loader = ({ width, height }: { width?: number; height?: number }) => {
  return <LoaderIcon width={width} height={height} className={cn.loader} />;
};

export default Loader;
