import Layout from "@/components/shared/Layout";
import { useCheckAuth } from "@/hooks/useCheckAuth";
import BalanceCard from "@/components/features/BalanceCard";
import Flex from "@/components/shared/Flex";
import Button from "@/components/shared/Button";
import { ButtonVariant } from "@/constants/button";

const BalancePage = () => {
  useCheckAuth();

  return (
    <Layout>
      <Flex direction="column" rowGap={24}>
        <BalanceCard />
        <Button variant={ButtonVariant.primary} wide>
          Вывести
        </Button>
      </Flex>
    </Layout>
  );
};

export default BalancePage;
