import Layout from "@/components/shared/Layout";
import { useCheckAuth } from "@/hooks/useCheckAuth";
import Flex from "@/components/shared/Flex";
import Heading from "@/components/shared/Heading";
import { useAppSelector } from "@/hooks/useAppSelector";
import {
  useLazyGetAvailableTasksQuery,
  useLazyGetExecutionTasksQuery,
} from "@/api/taskApi";
import { useEffect } from "react";
import SkeletonCard from "@/components/shared/Skeletons/SkeletonCard";
import TaskCard from "@/components/features/TaskCard";
import { Link } from "react-router-dom";
import { createTaskLink } from "@/utils/task";

const HomePage = () => {
  const { isAuthorized } = useAppSelector((state) => state.auth);

  useCheckAuth();

  const [
    getExecutionTasksTrigger,
    { data: executionTasks, isLoading: isExecutionTasksLoading },
  ] = useLazyGetExecutionTasksQuery();

  const [
    getAvailableTasksTrigger,
    { data: availableTasks, isLoading: isAvailableTasksLoading },
  ] = useLazyGetAvailableTasksQuery();

  useEffect(() => {
    if (isAuthorized) {
      getExecutionTasksTrigger();
      getAvailableTasksTrigger();
    }
  }, [isAuthorized]);

  return (
    <Layout>
      <Flex direction="column" rowGap={24}>
        <Flex direction="column" rowGap={16}>
          <Heading>Текущие задачи</Heading>
          {isExecutionTasksLoading ? (
            <SkeletonCard />
          ) : (
            executionTasks?.map((el, index) => (
              <Link
                key={`execution-task-card-home-${index}`}
                to={createTaskLink(el.id)}
              >
                <TaskCard
                  id={el.id}
                  status={el.status}
                  type={el.type}
                  title={el.title}
                  amount={el.amount}
                  dates={el.dates}
                  isTrackerVisible
                />
              </Link>
            ))
          )}

          <Heading>Доступные задачи</Heading>
          {isAvailableTasksLoading ? (
            <SkeletonCard />
          ) : (
            availableTasks?.map((el, index) => (
              <Link
                key={`available-task-card-home-${index}`}
                to={createTaskLink(el.id)}
              >
                <TaskCard
                  id={el.id}
                  status={el.status}
                  type={el.type}
                  title={el.title}
                  amount={el.amount}
                />
              </Link>
            ))
          )}
        </Flex>
      </Flex>
    </Layout>
  );
};

export default HomePage;
