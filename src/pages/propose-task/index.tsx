import Layout from "@/components/shared/Layout";
import { useCheckAuth } from "@/hooks/useCheckAuth";

const ProposeTaskPage = () => {
  useCheckAuth();

  return <Layout>ProposeTaskPage</Layout>;
};

export default ProposeTaskPage;
