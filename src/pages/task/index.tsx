import Layout from "@/components/shared/Layout";
import { useGetTaskByIdQuery } from "@/api/taskApi";
import { useParams } from "react-router-dom";
import { skipToken } from "@reduxjs/toolkit/query";
import Flex from "@/components/shared/Flex";
import Heading from "@/components/shared/Heading";
import { useCheckAuth } from "@/hooks/useCheckAuth";
import Loader from "@/components/shared/Loader";
import { toMoneyFormat } from "@/utils/format";
import { TASK_STATUS_TRANSLATE } from "@/constants/task";
import LazyIcon from "@/components/shared/LazyIcon";
import { IconName } from "@/constants/icons";
import { clsx } from "clsx";
import cn from "./styles.module.scss";

const TaskPage = () => {
  useCheckAuth();

  const { taskId } = useParams();

  const { data, isLoading } = useGetTaskByIdQuery(
    taskId ? { taskId } : skipToken,
  );

  return (
    <Layout>
      {isLoading ? (
        <div className={cn.loader}>
          <Loader height={32} width={32} />
        </div>
      ) : (
        <Flex direction="column" rowGap={24}>
          <Heading size={2}>Информация о задаче</Heading>
          <Flex direction="column" rowGap={8}>
            <Flex direction="column" rowGap={2}>
              <span className={cn.title}>Заголовок задачи:</span>
              <span className={cn.description}>{data?.title}</span>
            </Flex>

            {data?.description && (
              <Flex direction="column" rowGap={2}>
                <span className={cn.title}>Описание задачи:</span>
                <span
                  className={cn.description}
                  dangerouslySetInnerHTML={{ __html: data?.description }}
                />
              </Flex>
            )}

            <Flex direction="column" rowGap={2}>
              <span className={cn.title}>Стоимость задачи:</span>
              <Flex columnGap={4} alignItems="center">
                <span className={cn.description}>
                  {toMoneyFormat(data?.amount || 0)}
                </span>
                <LazyIcon name={IconName.cash} width={20} height={20} />
              </Flex>
            </Flex>

            {data?.status && (
              <Flex direction="column" rowGap={2}>
                <span className={cn.title}>Статус задачи:</span>
                <span
                  className={clsx(cn.description, cn[`status_${data.status}`])}
                >
                  {TASK_STATUS_TRANSLATE[data.status]}
                </span>
              </Flex>
            )}
          </Flex>
        </Flex>
      )}
    </Layout>
  );
};

export default TaskPage;
