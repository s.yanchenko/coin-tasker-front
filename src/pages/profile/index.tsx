import Layout from "@/components/shared/Layout";
import { useCheckAuth } from "@/hooks/useCheckAuth";
import { useGetProfileQuery } from "@/api/userApi";
import SkeletonCard from "@/components/shared/Skeletons/SkeletonCard";
import ProfileCard from "@/components/features/ProfileCard";
import Flex from "@/components/shared/Flex";
import Button from "@/components/shared/Button";
import { ButtonVariant } from "@/constants/button";
import { useLazyCheckAuthQuery, useLazyLogoutQuery } from "@/api/authApi";
import ProfileEditModal from "@/components/features/ProfileEditModal";
import { useState } from "react";

const ProfilePage = () => {
  useCheckAuth();

  const [isEditModalOpen, setIsEditModalOpen] = useState<boolean>(false);

  const { data, isLoading } = useGetProfileQuery();

  const [logoutTrigger] = useLazyLogoutQuery();

  const [checkAuthTrigger] = useLazyCheckAuthQuery();

  const handleLogout = () => {
    logoutTrigger().then(() => {
      checkAuthTrigger();
    });
  };

  return (
    <Layout>
      {isLoading ? (
        <SkeletonCard />
      ) : (
        data !== undefined && (
          <Flex direction="column" rowGap={24}>
            <ProfileCard
              nickname={data.nickname}
              balance={data.balance}
              avatar={data.avatar}
              setIsEditModalOpen={setIsEditModalOpen}
            />

            <Button variant={ButtonVariant.danger} onClick={handleLogout} wide>
              Выйти из аккаунта
            </Button>
          </Flex>
        )
      )}
      <ProfileEditModal
        nickname={data?.nickname}
        avatar={data?.avatar}
        isOpen={isEditModalOpen}
        setIsOpen={(value) => setIsEditModalOpen(value)}
      />
    </Layout>
  );
};

export default ProfilePage;
