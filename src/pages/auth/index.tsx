import Input from "@/components/shared/Input";
import Button from "@/components/shared/Button";
import { Controller, useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { useLoginMutation } from "@/api/authApi";
import Callout from "@/components/shared/Callout";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Pages } from "@/constants/routes/pages";
import { IconName } from "@/constants/icons";
import { AuthFormFields, LoginError } from "@/types/auth";
import { authScheme } from "@/domain/validation/auth/authScheme";
import cn from "./styles.module.scss";

const AuthPage = () => {
  const [isShowCallout, setIsShowCallout] = useState<boolean>(false);

  const { handleSubmit, control } = useForm<AuthFormFields>({
    defaultValues: {
      email: undefined,
      password: undefined,
    },
    resolver: zodResolver(authScheme),
    mode: "onBlur",
  });

  const navigate = useNavigate();

  const [loginTrigger, { error: apiError, isLoading, isSuccess }] =
    useLoginMutation();

  const handleSubmitForm = (data: AuthFormFields) => {
    loginTrigger({ loginBody: data });
  };

  useEffect(() => {
    if (apiError) {
      setIsShowCallout(true);
    }
  }, [apiError]);

  useEffect(() => {
    if (isSuccess) {
      navigate(Pages.home);
    }
  }, [isSuccess]);

  return (
    <div className={cn.container}>
      <div className={cn.wrapper}>
        {isShowCallout && (
          <div className={cn.callout}>
            <Callout
              title="Ошибка авторизации"
              description={(apiError as LoginError)?.data?.description}
              variant="danger"
              closeCallback={() => {
                setIsShowCallout(false);
              }}
            />
          </div>
        )}
        <form className={cn.form} onSubmit={handleSubmit(handleSubmitForm)}>
          <span className={cn.form_title}>Авторизация</span>
          <Controller
            control={control}
            name="email"
            render={({
              field: { onChange, onBlur, value },
              fieldState: { error },
            }) => (
              <Input
                type="email"
                placeholder="john@mail.com"
                label="Введите Email"
                iconName={IconName.userBold}
                onChange={onChange}
                onBlur={onBlur}
                value={value || ""}
                isInvalid={!!error || !!apiError}
                subText={error?.message}
              />
            )}
          />
          <Controller
            control={control}
            name="password"
            render={({
              field: { onChange, onBlur, value },
              fieldState: { error },
            }) => (
              <Input
                type="password"
                placeholder="Пароль"
                label="Введите пароль"
                iconName={IconName.lockClosedBold}
                onChange={onChange}
                onBlur={onBlur}
                value={value || ""}
                isInvalid={!!error || !!apiError}
                subText={error?.message}
              />
            )}
          />
          <Button type="submit" variant="primary" isLoading={isLoading} wide>
            Отправить
          </Button>
        </form>
      </div>
    </div>
  );
};

export default AuthPage;
