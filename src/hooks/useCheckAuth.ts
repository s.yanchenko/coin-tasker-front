import { useLazyCheckAuthQuery } from "@/api/authApi";
import { useLocation, useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { FetchBaseQueryError } from "@reduxjs/toolkit/query";
import { useAppSelector } from "@/hooks/useAppSelector";
import { useAppDispatch } from "@/hooks/useAppDispatch";
import { Pages } from "@/constants/routes/pages";
import { HttpCode } from "@/constants/api";
import { setIsAuthorized } from "@/slices/authSlice";

export const useCheckAuth = () => {
  const { isAuthorized } = useAppSelector((state) => state.auth);

  const dispatch = useAppDispatch();

  const location = useLocation();

  const navigate = useNavigate();

  const [checkAuthTrigger, { error, isSuccess }] = useLazyCheckAuthQuery();

  useEffect(() => {
    if (!isAuthorized) {
      checkAuthTrigger();
    }
  }, []);

  useEffect(() => {
    if ((error as FetchBaseQueryError)?.status === HttpCode.unauthorized) {
      navigate(Pages.auth);
    }
  }, [error]);

  useEffect(() => {
    if (isSuccess) {
      dispatch(setIsAuthorized(true));
      navigate(location.pathname);
    }
  }, [isSuccess]);
};
