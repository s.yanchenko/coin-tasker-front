import { useCallback, useState } from "react";
import {
  useChangeTrackerStatusMutation,
  useStartTaskByIdMutation,
} from "@/api/taskApi";
import { useCalculateTrackerTime } from "@/hooks/useCalculateTrackerTime";
import { ETaskStatus } from "@/constants/task";
import { TTaskStatus } from "@/types/task";

export const useTracker = ({
  id,
  dates,
  status,
}: {
  id: string;
  dates?: { start: Date; end: Date | null }[];
  status: TTaskStatus;
}) => {
  const [startTime, setStartTime] = useState<number>(0);

  const [updateTask] = useChangeTrackerStatusMutation();

  const [startTask] = useStartTaskByIdMutation();

  const { stopTimer, startTimer } = useCalculateTrackerTime({
    setStartTime,
    trackerDates: dates || [],
    taskStatus: status,
  });

  const handleUpdateTask = useCallback(() => {
    updateTask({
      taskId: id,
      body: { date: new Date().toISOString(), status },
    });
  }, [id, status, updateTask]);

  const handleStartTask = useCallback(() => {
    startTask({
      taskId: id,
      body: { date: new Date().toISOString() },
    });
  }, [id, startTask]);

  const handleClickControl = () => {
    if (status === ETaskStatus.progress) {
      stopTimer();
      handleUpdateTask();
    }
    if (status === ETaskStatus.paused) {
      startTimer();
      handleUpdateTask();
    }
    if (status === ETaskStatus.available) {
      handleStartTask();
    }
  };

  if (!dates || !dates?.length) {
    stopTimer();
  }

  return { startTime, handleClickControl };
};
