import {
  Dispatch,
  SetStateAction,
  useCallback,
  useEffect,
  useRef,
} from "react";
import { TTaskStatus } from "@/types/task";
import { ETaskStatus } from "@/constants/task";
import { getTrackerTime } from "@/utils/task";

export const useCalculateTrackerTime = ({
  setStartTime,
  trackerDates,
  taskStatus,
}: {
  setStartTime: Dispatch<SetStateAction<number>>;
  trackerDates: { start: Date; end: Date | null }[];
  taskStatus: TTaskStatus;
}) => {
  const trackerRef = useRef<ReturnType<typeof setInterval>>();

  const startTimer = useCallback(() => {
    if (trackerRef.current) {
      clearInterval(trackerRef.current);
    }

    trackerRef.current = setInterval(() => {
      setStartTime((prev) => (prev || 0) + 1000);
    }, 1000);
  }, [setStartTime]);

  useEffect(() => {
    setStartTime(getTrackerTime(trackerDates));

    if (taskStatus === ETaskStatus.paused) {
      return;
    }

    startTimer();

    return () => {
      clearInterval(trackerRef.current);
    };

    // eslint-disable-next-line
  }, [taskStatus]);

  const stopTimer = useCallback(() => {
    clearInterval(trackerRef.current);
  }, []);

  return { stopTimer, startTimer };
};
