export const toMoneyFormat = (amount: number) => new Intl.NumberFormat().format(amount);
