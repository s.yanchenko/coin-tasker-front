export const msToTimeString = (ms: number) =>
  new Date(2024, 0, 1, 0, 0, 0, ms).toLocaleTimeString();
