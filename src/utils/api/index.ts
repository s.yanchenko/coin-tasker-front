export const createApiBaseUrl = (domain: string) =>
  `${import.meta.env.VITE_API_URL}/api/${domain}`;
