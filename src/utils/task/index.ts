import { Pages } from "@/constants/routes/pages";

export const getTrackerTime = (
  trackerDates: { start: Date; end: Date | null }[],
) => {
  const currentDate = new Date();

  return trackerDates.reduce((previousValue, currentValue) => {
    if (currentValue.end === null) {
      return (
        previousValue +
        Math.abs(currentDate.getTime() - currentValue.start.getTime())
      );
    }
    return (
      previousValue +
      Math.abs(currentValue.end.getTime() - currentValue.start.getTime())
    );
  }, 0);
};

export const createTaskLink = (taskId: string) => `${Pages.task}/${taskId}`;
