import ReactDOM from "react-dom/client";
import { RouterProvider } from "react-router-dom";
import { router } from "@/constants/routes/router";
import { store } from "@/store";
import { Provider } from "react-redux";
import "@/styles/global.scss";
import SkeletonPage from "@/components/shared/Skeletons/SkeletonPage";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <Provider store={store}>
    <RouterProvider router={router} fallbackElement={<SkeletonPage />} />
  </Provider>,
);
