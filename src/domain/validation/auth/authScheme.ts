import { z } from "zod";
import { InvalidEmailError, RuleDefault } from "@/constants/exceptions";

export const authScheme = z.object({
  email: z.string(RuleDefault).email(InvalidEmailError),
  password: z
    .string(RuleDefault)
    .min(8, "Минимальная длина пароля 8 символов")
    .max(42, "Максимальная длина пароля 42 символа"),
});
