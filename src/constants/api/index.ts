export enum HttpCode {
  success = 200,
  badRequest = 400,
  unauthorized = 401,
}
