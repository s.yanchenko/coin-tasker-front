export const RequiredError = 'Обязательное поле';

export const InvalidTypeError = 'Неверные данные';

export const RuleDefault = {
  invalid_type_error: InvalidTypeError,
  required_error: RequiredError,
};

export const InvalidEmailError = 'Введите правильный Email';
