export enum ETaskType {
  book = "book",
  fitness = "fitness",
  video = "video",
  telegram = "telegram",
  film = "film",
}

export enum ETaskStatus {
  available = "available",
  done = "done",
  paused = "paused",
  progress = "progress",
}

export const TASK_STATUS_TRANSLATE = {
  available: "Доступна",
  done: "Выполнена",
  paused: "На паузе",
  progress: "В процессе выполнения",
};
