export const Pages = {
  home: "/",
  balance: "/balance",
  tasks: "/tasks",
  proposeTask: "/propose-task",
  profile: "/profile",
  task: "/task",
  adminPanel: "/admin-panel",
  auth: "/auth",
} as const;
