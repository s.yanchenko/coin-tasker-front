import { lazy, Suspense } from "react";
import { createBrowserRouter } from "react-router-dom";
import SkeletonPage from "@/components/shared/Skeletons/SkeletonPage";
import { Pages } from "@/constants/routes/pages";
import NoMatchPage from "@/pages/no-match";

const LazyAdminPanelPage = lazy(() => import("@/pages/admin-panel"));
const LazyBalancePage = lazy(() => import("@/pages/balance"));
const LazyHomePage = lazy(() => import("@/pages/home"));
const LazyProfilePage = lazy(() => import("@/pages/profile"));
const LazyProposeTaskPage = lazy(() => import("@/pages/propose-task"));
const LazyTasksPage = lazy(() => import("@/pages/tasks"));
const LazyAuthPage = lazy(() => import("@/pages/auth"));
const LazyTaskPage = lazy(() => import("@/pages/task"));

export const router = createBrowserRouter([
  {
    path: Pages.adminPanel,
    element: (
      <Suspense fallback={<SkeletonPage />}>
        <LazyAdminPanelPage />
      </Suspense>
    ),
  },
  {
    path: Pages.balance,
    element: (
      <Suspense fallback={<SkeletonPage />}>
        <LazyBalancePage />
      </Suspense>
    ),
  },
  {
    path: Pages.home,
    element: (
      <Suspense fallback={<SkeletonPage />}>
        <LazyHomePage />
      </Suspense>
    ),
  },
  {
    path: Pages.profile,
    element: (
      <Suspense fallback={<SkeletonPage />}>
        <LazyProfilePage />
      </Suspense>
    ),
  },
  {
    path: Pages.proposeTask,
    element: (
      <Suspense fallback={<SkeletonPage />}>
        <LazyProposeTaskPage />
      </Suspense>
    ),
  },
  {
    path: Pages.tasks,
    element: (
      <Suspense fallback={<SkeletonPage />}>
        <LazyTasksPage />
      </Suspense>
    ),
  },
  {
    path: Pages.auth,
    element: (
      <Suspense>
        <LazyAuthPage />
      </Suspense>
    ),
  },
  {
    path: `${Pages.task}/:taskId`,
    element: (
      <Suspense>
        <LazyTaskPage />
      </Suspense>
    ),
  },
  {
    path: "*",
    element: <NoMatchPage />,
  },
]);
