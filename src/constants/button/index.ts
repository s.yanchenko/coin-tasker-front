export const ButtonVariant = {
  primary: "primary",
  secondary: "secondary",
  danger: "danger",
  ghost: "ghost",
  hollow: "hollow",
} as const;

export const ButtonPadding = {
  relaxed: "relaxed",
  compact: "compact",
} as const;

export const ButtonFontSize = {
  xs: "xs",
  sm: "sm",
  base: "base",
  lg: "lg",
} as const;
