import { IconName } from "@/constants/icons";

export type TIconName = keyof typeof IconName;
