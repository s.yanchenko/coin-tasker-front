export type ButtonVariantType =
  | "primary"
  | "secondary"
  | "danger"
  | "ghost"
  | "hollow";
