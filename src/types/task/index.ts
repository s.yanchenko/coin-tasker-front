import { ETaskStatus, ETaskType } from "@/constants/task";

export type TTaskType = `${ETaskType}`;

export type TTaskStatus = `${ETaskStatus}`;
