export type LoginError = {
  data: {
    description: string;
  };
};

export type AuthFormFields = {
  email: string;
  password: string;
};
