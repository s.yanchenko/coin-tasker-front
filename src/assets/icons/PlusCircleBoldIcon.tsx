const PlusCircleBoldIcon = ({ width, height, className }: { width?: number; height?: number; className?: string }) => {
  return (
    <svg
      width={width}
      height={height}
      className={className}
      viewBox='0 0 24 24'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M11.9999 21.6C17.3018 21.6 21.5999 17.302 21.5999 12C21.5999 6.69809 17.3018 2.40002 11.9999 2.40002C6.69797 2.40002 2.3999 6.69809 2.3999 12C2.3999 17.302 6.69797 21.6 11.9999 21.6ZM13.1999 8.40002C13.1999 7.73728 12.6626 7.20002 11.9999 7.20002C11.3372 7.20002 10.7999 7.73728 10.7999 8.40002V10.8H8.3999C7.73716 10.8 7.1999 11.3373 7.1999 12C7.1999 12.6628 7.73716 13.2 8.3999 13.2H10.7999V15.6C10.7999 16.2628 11.3372 16.8 11.9999 16.8C12.6626 16.8 13.1999 16.2628 13.1999 15.6V13.2H15.5999C16.2626 13.2 16.7999 12.6628 16.7999 12C16.7999 11.3373 16.2626 10.8 15.5999 10.8H13.1999V8.40002Z'
        fill='currentColor'
      />
    </svg>
  );
};

export default PlusCircleBoldIcon;
