const PencilBoldIcon = ({
  width,
  height,
  className,
}: {
  width?: number;
  height?: number;
  className?: string;
}) => {
  return (
    <svg
      width={width}
      height={height}
      className={className}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M16.303 4.30292C17.2403 3.36566 18.7599 3.36566 19.6972 4.30292C20.6344 5.24018 20.6344 6.75977 19.6972 7.69703L18.7457 8.6485L15.3516 5.25439L16.303 4.30292Z"
        fill="currentColor"
      />
      <path
        d="M13.6545 6.95145L3.6001 17.0059V20.4H6.99421L17.0486 10.3456L13.6545 6.95145Z"
        fill="currentColor"
      />
    </svg>
  );
};

export default PencilBoldIcon;
